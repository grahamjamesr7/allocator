// -----------------------------------------
// projects/c++/allocator/TestAllocator1.c++
// Copyright (C) 2017
// Glenn P. Downing
// -----------------------------------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/Primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/AdvancedGuide.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator
#include <iostream>
#include <list>

#include "gtest/gtest.h"

#include "Allocator.h"


// --------------
// TestAllocator1
// --------------
TEST(TestAllocator0, basic_test){

  my_allocator<int, 100> x;
  const int      s = 1;
  const int     v = 2;
  int*        p = x.allocate(s);
  if (p != nullptr) {
      x.construct(p, v);
      ASSERT_EQ(v, *p);
      x.destroy(p);
      x.deallocate(p, s);
  }
}


template <typename A>
struct TestAllocator1 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>
    my_types_1;

TYPED_TEST_CASE(TestAllocator1, my_types_1);

TYPED_TEST(TestAllocator1, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type      s = 1;
    const value_type     v = 2;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator1, test_10) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type  x;
    const size_type       s = 10;
    const value_type      v = 2;
    const pointer         b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// --------------
// TestAllocator2
// --------------



TEST(TestAllocator2, bad_alloc) {
    try
    {
        my_allocator<int, 5> x;
    } catch(std::bad_alloc e)
    {
        std::cout<<e.what()<<std::endl;
        assert(strcmp("std::bad_alloc",e.what()) == 0);
        //ASSERT_EQ("std::bad_alloc",e.what());
    }
}

// --------------
// TestAllocator3
// --------------

template <typename A>
struct TestAllocator3 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
my_allocator<int,    100>,
             my_allocator<double, 100>>
             my_types_2;

TYPED_TEST_CASE(TestAllocator3, my_types_2);

TYPED_TEST(TestAllocator3, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type      s = 1;
    const value_type     v = 2;
    const pointer        p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator3, test_10) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type      s = 10;
    const value_type     v = 2;
    const pointer        b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// --------------
// TestAllocator4
// --------------

//more user tests

TEST(TestAllocator4, mult_alloc) {
    my_allocator<double, 100> x;
    double* p1 = x.allocate(2);
    x.allocate(2);
    x.allocate(2);
    double* p2 = x.allocate(2);
    //72 bytes, equal to 9 doubles
    ASSERT_EQ(p2 - p1, 9);
}

TEST(TestAllocator4, dealloc_alloc) {
    my_allocator<double, 100> x;
    double* p1 = x.allocate(2);
    x.deallocate(p1, sizeof(double));
    x.allocate(2);
    x.allocate(2);
    double* p2 = x.allocate(2);
    //72 bytes, equal to 6 doubles
    ASSERT_EQ(p2 - p1, 6);
}

TEST(TestAllocator4, large_alloc) {
    my_allocator<double, 100> x;
    double* p1 = x.allocate(11);
    x.deallocate(p1, sizeof(double));
    ASSERT_EQ(x.observe(), "92");
}

TEST(TestAllocator4, chars) {
    my_allocator<char, 100> x;
    x.allocate(5);
    x.allocate(5);
    x.allocate(5);
    x.allocate(5);
    x.allocate(5);
    x.allocate(5);
    ASSERT_EQ(x.observe(), "-5 -5 -5 -5 -5 -5 14");
}

TEST(TestAllocator4, strings) {
    my_allocator<char*, 100> x;
    x.allocate(1);
    x.allocate(1);
    x.allocate(1);
    x.allocate(1);
    x.allocate(1);
    x.allocate(1);
    ASSERT_EQ(x.observe(), "-8 -8 -8 -8 -8 -12");
}

TEST(TestAllocator4, large_alloc2) {
    my_allocator<double, 23> x;
    x.allocate(1);
    ASSERT_EQ(x.observe(), "-15");

}

TEST(TestAllocator4, internal_dealloc) {
    my_allocator<double, 100> x;
    x.allocate(1);
    double* p = x.allocate(1);
    x.allocate(1);
    x.deallocate(p, sizeof(double));
    ASSERT_EQ(x.observe(), "-8 8 -8 44");
}

TEST(TestAllocator4, internal_realloc) {
    my_allocator<double, 100> x;
    x.allocate(1);
    double* p = x.allocate(1);
    x.allocate(1);
    x.deallocate(p, sizeof(double));
    x.allocate(1);
    ASSERT_EQ(x.observe(), "-8 -8 -8 44");
}

TEST(TestAllocator4, many_alloc_dealloc) {
    my_allocator<char, 1000> x;
    list<char*> pointers;
    for(int i = 0; i < 20; ++i){
        pointers.push_back(x.allocate(1));
    }
    for(int i = 0; i < 20; ++i){
        x.deallocate(pointers.front(), sizeof(char));
        pointers.pop_front();
    }
    ASSERT_EQ(x.observe(), "992");
}

TEST(TestAllocator4, no_alloc) {
    my_allocator<double, 1000> x;
    ASSERT_EQ(x.observe(), "992");
}
