// ----------------------------------
// projects/c++/allocator/Allocator.h
// Copyright (C) 2018
// Glenn P. Downing
// ----------------------------------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <string>
#include <iostream>


using namespace std;
// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     *
     *
     *
     */
    bool valid () const
    {
        int i = 0;
        int sentinel = 0;

        do
        {
            //start at the first sentinel
            sentinel = (*this)[i];

            i += abs(sentinel) + 4;

            //sentinel 1 and sentinel 2 don't match
            if((*this)[i] != sentinel){
                return false;
              }

            //we've encountered the last sentinel and can stop checking things now
            if(i + 4 == N)
                break;

            //found two free blocks next to each other
            if((*this)[i] > 0 && (*this)[i+4] > 0){
                return false;
              }

            //gone beyond the end of the array
            if(i > N){
                return false;
              }

            i += 4;

        } while(i < N);

        return true;
    }

public:
    // ------------
    // constructors
    // ------------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator ()
    {
        //don't ask for an allocator that can't make at least 1 block
        if(N < sizeof(T) + (2 * sizeof(int)))
            throw std::bad_alloc();

        //set initial sentinels
        int free_space = N - (2 * sizeof(int));
        (*this)[0] = free_space;
        (*this)[N-sizeof(int)] = free_space;

        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type request)
    {
        request *= sizeof(T); //num of T to size to hold that many T's

        int i = 0;
        int sentinel = 0;
        int new_sentinel = -1;
        int next_free_sentinel = 0;
        int leftover_space = 0;
        T* free_space_pointer;

        //locate first positive sentinel
        while((sentinel = (*this)[i]) < 0 || sentinel < request)
            i += abs(sentinel) +8;

        //check if we have enough space
        leftover_space = (sentinel - request);
        if(leftover_space < sizeof(T) + (2*sizeof(int)))
            request += leftover_space;
        else
            new_sentinel = leftover_space;

        //in the slot we found, insert the updated sentinel value, then continue and create a new sentinel value
        (*this)[i] = -1*request;
        free_space_pointer = get_free_space_pointer(i + 4);
        (*this)[i += 4+request] = -1*request;

        //if there's enough space
        if(new_sentinel > 0)
        {
            (*this)[i+=4] = next_free_sentinel = (sentinel - (request+8));
            (*this)[i+=next_free_sentinel+4] = next_free_sentinel;
        }

        assert(valid());
        return free_space_pointer;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());                         // from the prohibition of new
    }


    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks are coalesced
     * throws an invalid_argument exception, if p is invalid
     */
    void deallocate (pointer point, size_type size)
    {
        //get the byte number to deallocate
        int i = (point - (T*)&((*this)[0]))*sizeof(T);
        //to get the sentinel before the value
        i -= 4;
        if((*this)[i] > 0)
            throw invalid_argument("Invalid argument to deallocate");

        int sentinel = abs((*this)[i]);
        (*this)[i] = sentinel;
        (*this)[i + sentinel + 4] = sentinel;

        if(i > 0)
        {
            int prev_sentinel = (*this)[i - 4];
            if(prev_sentinel > 0)
                i = coalesce(i - prev_sentinel - 8, i);
        }

        int new_i = i + (*this)[i] + 8;
        if(new_i < N && (*this)[new_i] > 0)
            i = coalesce(i, new_i);

        assert(valid());
    }


    /**
     * O(1) in space
     * O(1) in time
     * takes two blocks, left and right, and combines them into one free block
     * @return the left index, now the index of the combined free block
     */
    int coalesce (int left, int right)
    {
        int* left_sentinel = &(*this)[left];
        int* right_sentinel = &(*this)[right];
        int new_sentinel = *left_sentinel + *right_sentinel + 8;
        *left_sentinel = new_sentinel;
        (*this)[right + *right_sentinel + 4] = new_sentinel;
        return left;
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -------
    // observe
    // -------

    /**
     * O(1) in space
     * O(n) in time
     *
     * @return a string that represents the current state of memory
     */
    const string observe () const
    {

        string ret = "";
        int i = 0;
        int sentinel = 0;
        int abs_sent = 0;
        do
        {
            sentinel = (*this)[i];
            abs_sent = abs(sentinel);

            ret += to_string(sentinel) + " ";
            i+= abs_sent + 8;

        } while(i != N);
        //cut off the last space
        return ret.substr(0, ret.size() - 1);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    T* get_free_space_pointer (int i) {
        return reinterpret_cast<T*>(&a[i]);
    }
};

#endif // Allocator_h
