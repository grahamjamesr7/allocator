// ---------------------------------------
// projects/c++/allocator/RunAllocator.c++
// Copyright (C) 2018
// Glenn P. Downing
// ---------------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <istream>  // istream
#include <iostream> ////cout, endl, getline
#include <string>   // s
#include <vector>
#include <list>
#include <algorithm>

#include "Allocator.h"



using namespace std;


void allocator_solve (istream& r, ostream& w)
{
    string s;
    int num_trials = 0;
    int request = 0;
    vector<double*> pointers;

    getline(r,s);
    num_trials = stoi(s);
    getline(r,s);

    for(int i = 0; i < num_trials; ++i)
    {
        //reinit the allocator and pointers on each run
        my_allocator<double, 1000> allocator;
        pointers.clear();

        while (getline(r, s) && s != "")
        {
            request = stoi(s);

            //request for deallocate
            if(request < 0)
            {
                //convert dealloc and do it
                if(!pointers.empty() && pointers.size() >= abs(request))
                {
                    int index = abs(request) - 1;
                    allocator.deallocate(pointers[index], sizeof(double));
                    pointers.erase(pointers.begin() + index);
                }
            }
            else
            {
                //allocate and store the pointer
                pointers.push_back(allocator.allocate(request));
                sort(pointers.begin(),pointers.end());

            }
        }
        //print out the ending state of the memory
        w << allocator.observe() << endl;
    }
}

// ----
// main
// ----

int main ()
{
    allocator_solve(cin,cout);
}
